# chat-socketio

Estudo sobre socket io utilizando nodejs, html, javascript, ejs, express e o principal socket.io

# Requisitos

A máquina deverá estar devidamente instalada com o Node versão 12 ou superior.

# Como executar

no diretório onde encontra-se o arquivo server.js, executar o seguinte comando na linha de comandos :
```node server.js```
